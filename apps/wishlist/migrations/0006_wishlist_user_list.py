# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-08-27 05:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wishlist', '0005_auto_20180826_2242'),
    ]

    operations = [
        migrations.AddField(
            model_name='wishlist',
            name='user_list',
            field=models.ManyToManyField(to='wishlist.User'),
        ),
    ]
