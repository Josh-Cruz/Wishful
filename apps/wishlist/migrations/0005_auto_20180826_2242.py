# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-08-27 05:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wishlist', '0004_auto_20180826_2020'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userwishlist',
            name='users',
        ),
        migrations.RemoveField(
            model_name='userwishlist',
            name='wished_for',
        ),
        migrations.DeleteModel(
            name='UserWishlist',
        ),
    ]
