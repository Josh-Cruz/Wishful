from django.conf.urls import url
from . import views           # This line is new!
urlpatterns = [
    url(r'^$', views.index),     # This line has changed!
    url(r'^process_user/$', views.process_user),    # This line has changed!
    url(r'^dashboard/$', views.dashboard),  
    url(r'^logout/$', views.logout),  
    url(r'^wish_items/create', views.create),
    url(r'^wish_items/new_item', views.new_item),
    url(r'^wish_items/(?P<item_id>\d+)/add_item', views.add_item_to_my_wishlist),
    url(r'^wish_items/(?P<item_id>\d+)/remove', views.remove),
    url(r'^wish_items/(?P<item_id>\d+)/delete', views.delete),
    url(r'^wish_items/(?P<item_id>\d+)', views.display),
   

    # url(r'^$', views.index, name='index'),
    # url(r'^create/$', views.create, name="create"),
    # url(r'^new/$', views.new, name="new"),
    # url(r'^(?P<user_id>\d+)/show$', views.show, name="show"),
    # url(r'^logout/$', views.logout, name='logout'),
    # url(r'^login/$', views.login, name='login'),
    # url(r'^(?P<user_id>\d+)/edit', views.edit, name="edit"),
    # url(r'^(?P<user_id>\d+)/delete', views.delete, name="delete"),
    # url(r'^(?P<user_id>\d+)/update', views.update, name="update"),

]
