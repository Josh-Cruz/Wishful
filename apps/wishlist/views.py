# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *

# Create your views here.
def index(request):
    return render(request, "wishlist/index.html")

def register(request):
    return render(request, "wishlist/register.html")

def create(request):
    if "user_id" in request.session:
        return render(request, "wishlist/create.html")
    else:
        return redirect('/')    
        
def new_item(request):
    if request.method == "POST":
        results = wishlist.objects.validate_item(request.POST)
        if isinstance(results, dict):
            for tag, error in results.iteritems():
                messages.error(request, error, extra_tags=tag)
            return redirect('/wish_items/create')
        else:
            item = wishlist.objects.create(item_name=request.POST['item_name'], added_by=User.objects.get(id = request.session['user_id']))
            user = User.objects.get(id = request.session['user_id'])
            item.user_list.add(user)
            return redirect("/dashboard")

def add_item_to_my_wishlist(request, item_id):
    if request.method == "POST":
            item = wishlist.objects.get(id = item_id)
            user = User.objects.get(id = request.session['user_id'])
            item.user_list.add(user)
            return redirect("/dashboard")
    else:
        return redirect("/dashboard")
        

def dashboard(request):
    if "user_id" in request.session:
        user = User.objects.get(id = request.session['user_id'])
        context = {
            'first_name': user.first_name,
            'id': user.id,
            'users': User.objects.all(),
            "wishlist": wishlist.objects.all(),
            "my_list": wishlist.objects.filter (user_list__id= user.id )
        }
        return render(request, "wishlist/dashboard.html", context)
    else:
        return redirect('/')

def display(request, item_id):
    if "user_id" in request.session:
        item = wishlist.objects.get(id = item_id)
        context = {
            "name": item.item_name,
            "user_list": item.user_list.all(),
        }
        return render(request, "wishlist/display.html", context)
    else:
        return redirect('/')

    
def process_user(request):
    if request.method == "POST":
        if "Register" in request.POST:
            results = User.objects.basic_validator(request.POST)
            if isinstance(results, dict):
                for tag, error in results.iteritems():
                    messages.error(request, error, extra_tags=tag)
                return redirect('/')
            else:
                
                request.session['user_id'] = results
                return redirect('/dashboard')
        if "Login" in request.POST:
            results = User.objects.login_user(request.POST)
    
            if isinstance(results, dict):
                for tag, error in results.iteritems():
                    messages.error(request, error, extra_tags=tag)
                return redirect('/')
            else:
                request.session['user_id'] = results
                return redirect('/dashboard')
        else:
            return redirect('/')

def remove(request, item_id):
    if request.method == "POST":
            item = wishlist.objects.get(id = item_id)
            user = User.objects.get(id = request.session['user_id'])
            item.user_list.remove(user)
            return redirect("/dashboard")
    else:
        return redirect("/dashboard")

def delete(request, item_id):
    if "user_id" in request.session:
        item = wishlist.objects.get(id = item_id)
        item.delete()
        return redirect('/dashboard/')
    else:
        return redirect('/dashboard')
    
def logout(request):
  request.session.clear()
  return redirect('/')
    